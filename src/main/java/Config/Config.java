package Config;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.aeonbits.owner.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Config {

    @Getter
    @Setter
    private static String localUrl;

    @Getter
    @Setter
    private static String googleUrl;

    @Getter
    @Setter
    private static String sqlUrl;

    @Getter
    @Setter
    private static Logger logger;

    static {
        loadConfig();
    }

    public static void loadConfig() {
        var config = ConfigFactory.create(ConfigProject.class, System.getProperties());

        localUrl = config.url();
        googleUrl = config.googleUrl();
        sqlUrl = config.sqlUrl();
        logger = LoggerFactory.getLogger("SampleLogger");
    }
}
