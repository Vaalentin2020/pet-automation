package Config;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "system:properties",
        "classpath:urls.properties"})
public interface ConfigProject extends Config {

    @Key("local.url")
    String url();

    @Key("google.url")
    String googleUrl();

    @Key("sql.url")
    String sqlUrl();
}
