package APITests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static APITests.requestspec.SpecHelper.requestWithoutBody;
import static APITests.requestspec.SpecHelper.responseSpec;
import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_OK;

class FirstApiTest {

    @Test
    @DisplayName("Get all rockets from spacex")
    void getAllRockets() {
        given()
                .when()
                .spec(requestWithoutBody())
                .log().all()
                .get("/v3/rockets")
                .then().log().all()
                .statusCode(200);
    }

    @Test
    void getFirstThreeRockets() {
        var response = given()
                .when()
                .spec(requestWithoutBody())
                .log().all()
                .formParam("limit", 3)
                .get("/v3/rockets")
                .then()
                .spec(responseSpec(SC_OK))
                .extract().response();
        ArrayList<String> ids = response.jsonPath().get("id");
        Assertions.assertTrue(ids.size() >= 3);
    }
}
