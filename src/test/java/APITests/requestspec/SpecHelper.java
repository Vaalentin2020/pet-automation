package APITests.requestspec;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class SpecHelper {

    public static RequestSpecification requestWithoutBody() {
        return new RequestSpecBuilder()
                .setBaseUri("https://api.spacexdata.com")
                .addFilter(new AllureRestAssured())
                .build();
    }

    public static ResponseSpecification responseSpec(int status) {
        return new ResponseSpecBuilder()
                .expectStatusCode(status)
                .build();
    }
}
