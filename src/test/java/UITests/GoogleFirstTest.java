package UITests;

import Config.Config;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.AllureId;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

class GoogleFirstTest extends BaseTest {

    @Test
    @DisplayName("Open google and check the title name")
    @Severity(SeverityLevel.CRITICAL)
    @AllureId("T-001")
    void openGoogleTest() {
        Selenide.open(Config.getGoogleUrl());
        $(byText("Google")).should(exist);
    }

    @Test
    @DisplayName("Fill in a searching field and check if name is presented")
    @Severity(SeverityLevel.CRITICAL)
    @AllureId("T-002")
    void searchGoogle() {
        Selenide.open(Config.getGoogleUrl());
        $("[title='Search']").shouldBe(visible).setValue("1234");
        $("[aria-label='Google Search']").click();
        $(byText("Feist - 1234 (Director's Version)")).should(visible);
    }
}