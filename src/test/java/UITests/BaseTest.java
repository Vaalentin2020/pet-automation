package UITests;

import Config.Config;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;

import static core.Browsers.CHROME;

public abstract class BaseTest {

    @BeforeAll
    static void init() {
        Config.loadConfig();
        setUp();
    }

    private static void setUp() {
        setUpDriver();
        setRemoteDriverIfNeeded();
        configureDriver();
    }

    private static void setUpDriver() {
        if (System.getProperty("selenide.browser").equals(CHROME)) {
            WebDriverManager.chromedriver().setup();
        } else {
            WebDriverManager.safaridriver().setup();
        }
    }

    private static void setRemoteDriverIfNeeded() {
        if (System.getProperty("remote").equals("true")) Configuration.remote = Config.getLocalUrl();
    }

    //TODO Load these configurations via OWNER Config class
    private static void configureDriver() {
        Configuration.browserSize = "1920x1080";
        Configuration.headless = false;
        Configuration.timeout = 15000;
    }

    @AfterEach
    void closeDriver() {
        Selenide.closeWebDriver();
    }
}
