package sql;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.sql.ResultSet;

class SqlCreateTest extends SqlBase {

    @SneakyThrows
    @Test
    void getAllAlbumFieldsTest() {
        var statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM Album");

        while (result.next()) {
            System.out.println(result.getInt("AlbumId") + "-->>>>" + result.getString("Title"));
        }
    }
}