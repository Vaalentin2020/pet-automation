package sql;

import Config.Config;
import UITests.BaseTest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.sql.*;

@Slf4j
public class SqlBase extends BaseTest {

    protected static Connection connection;

    @SneakyThrows
    @BeforeEach
    public void base() {
        Class.forName("com.mysql.cj.jdbc.Driver");
        System.out.println("Establishing connection");
        connection = DriverManager.getConnection(Config.getSqlUrl());
        System.out.println("Successfully connected");
    }

    @SneakyThrows
    public static Statement getStatement() {
        return connection.createStatement();
    }

    @SneakyThrows
    public static PreparedStatement getPreparedStatement(String preparedStatement) {
        return connection.prepareStatement(preparedStatement);
    }

    @SneakyThrows
    public static CallableStatement getCallableStatement(String callableStatement) {
        return connection.prepareCall(callableStatement);
    }

    @AfterEach
    @SneakyThrows
    public void quit() {
        connection.close();
        System.out.println("Connection closed");
    }
}
