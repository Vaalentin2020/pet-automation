package sql;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

class ResultMetaTest extends SqlBase {

    @SneakyThrows
    @Test
    void getResultMetaTest() {
        var statement = getStatement();
        var resultSet = statement.executeQuery("SELECT * FROM workers");
        var resultMetaData = resultSet.getMetaData();
        print(resultMetaData);
    }

    @SneakyThrows
    private void print(ResultSetMetaData resultMeta) {
        System.out.println("Table has " + resultMeta.getColumnCount() + " columns where workers name column called: " + resultMeta.getColumnName(2));
        System.out.println("Column 1 is autoincrement: " + resultMeta.isAutoIncrement(1));
    }

}
