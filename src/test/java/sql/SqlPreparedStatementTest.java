package sql;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

class SqlPreparedStatementTest extends SqlBase {

    @SneakyThrows
    @Test
    void insertPreparedStatement() {
        var prepStatement = getPreparedStatement("INSERT into workers values(?, ?, ?)");
        prepStatement.setInt(1, 103);
        prepStatement.setString(2, "Jane");
        prepStatement.setInt(3, 121454578);
        var i = prepStatement.executeUpdate();
        System.out.println("Affected rows: " + i);
    }

    @SneakyThrows
    @Test
    void updatePreparedStatement() {
        var prepStatement = getPreparedStatement("UPDATE workers SET emp_salary = ? WHERE emp_id = ?");
        prepStatement.setInt(1, 7777777);
        prepStatement.setInt(2, 103);
        var i = prepStatement.executeUpdate();
        System.out.println("Affected rows: " + i);
    }

    @SneakyThrows
    @Test
    void deletePreparedStatement() {
        var prepStatement = getPreparedStatement("DELETE FROM workers WHERE emp_id = ?");
        prepStatement.setInt(1, 101);
        var i = prepStatement.executeUpdate();
        System.out.println("Affected rows: " + i);
    }
}
