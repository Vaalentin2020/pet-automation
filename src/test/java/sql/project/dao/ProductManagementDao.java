package sql.project.dao;

import lombok.SneakyThrows;
import sql.SqlBase;
import sql.project.pojo.Product;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ProductManagementDao extends SqlBase {

    @SneakyThrows
    public List<Product> getAllProducts() {
        List<Product> list = new ArrayList<>();
        var statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM product");

        while (result.next()) {
            var product = new Product(result.getString("prod_id"), result.getString("prod_name"),
                    result.getInt("prod_price"));
            list.add(product);
        }
        quit();
        return list;
    }

    public Product getProductById(String id) {
        return getAllProducts().stream().filter(s -> id.equals(s.getProductId())).findFirst().orElseThrow();
    }

    @SneakyThrows
    public int addProduct(Product product) {
        var statement = getPreparedStatement("INSERT INTO product(prod_id, prod_name, prod_price) values(?, ?, ?)");
        statement.setString(1, product.getProductId());
        statement.setString(2, product.getProductName());
        statement.setInt(3, product.getProductPrice());

        return statement.executeUpdate();
    }

    @SneakyThrows
    public int updateProduct(Product product) {
        var statement = getPreparedStatement("UPDATE product SET prod_name=?,prod_price=? WHERE prod_id=?");
        statement.setString(1, product.getProductName());
        statement.setInt(2, product.getProductPrice());
        statement.setString(3, product.getProductId());

        return statement.executeUpdate();
    }
}
