package sql.project.application;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sql.SqlBase;
import sql.project.dao.ProductManagementDao;
import sql.project.pojo.Product;

class ApplicationInvokerTest extends SqlBase {

    private final ProductManagementDao productManagementDao = new ProductManagementDao();
    Product product1 = new Product("1", "soap", 100);
    Product product2 = new Product("1", "coal", 200);


    @Test
    void updateProducts() {
        Assertions.assertTrue(productManagementDao.updateProduct(product2) > 0);
    }

    @Test
    void insertProducts() {
        Assertions.assertEquals(1, productManagementDao.addProduct(product1));
    }

    @Test
    void viewAllProducts() {
        var products = productManagementDao.getAllProducts();
        for (Product s : products) {
            System.out.println("ID = " + s.getProductId() + "\n" +
                    "NAME = " +s.getProductName() + "\n" +
                    "PRICE = " + s.getProductPrice() + "\n\n");
        }
    }
}
