package sql.project.pojo;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Product {

    private String productId;
    private String productName;
    private Integer productPrice;
}
