package sql;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;

class SqlFileTest extends SqlBase {

    @SneakyThrows
    @Test
    void insertImageTest() {
        var file = Path.of("/Users/valentingavrilov/IdeaProjects/PetAutomation/src/main/resources/pngQrSecondSlide.png").toFile();
        Assertions.assertTrue(file.isFile());
        var fileInputStream = FileUtils.openInputStream(file);
        var prepStatement = getPreparedStatement("INSERT INTO imagetest VALUES(?, ?)");
        prepStatement.setString(1, "FileName");
        prepStatement.setBinaryStream(2, fileInputStream);

        var i = prepStatement.executeUpdate();
        System.out.println("Affected rows: " + i);
    }
}
