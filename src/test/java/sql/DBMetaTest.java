package sql;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.sql.DatabaseMetaData;

class DBMetaTest extends SqlBase {

    @SneakyThrows
    @Test
    void getResultMetaTest() {
        DatabaseMetaData dbmd = connection.getMetaData();
        print(dbmd);
    }

    @SneakyThrows
    private void print(DatabaseMetaData dbmd) {
        System.out.println("Driver - " + dbmd.getDriverName());
        System.out.println("Version - " + dbmd.getDriverVersion());
        System.out.println("Url - " + dbmd.getURL());
        System.out.println("Store Procedures - " + dbmd.supportsStoredProcedures());
        System.out.println("UserName - " + dbmd.getUserName());
    }
}
