package sql;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

class SqlInsertUpdateDeleteTest extends SqlBase {

    @SneakyThrows
    @Test
    void createTable() {
        var i = getStatement().executeUpdate("CREATE TABLE imagetest(image_name varchar2(100), image_content blob)");
        System.out.println("Affected rows: " + i);
    }

    @SneakyThrows
    @Test
    void insert() {
        //var i = statement.executeUpdate("INSERT into workers values(100, 'Ben', 120000)");
        var i = getStatement().executeUpdate("INSERT into workers(emp_id, emp_name) values(103, 'Mick')");
        System.out.println("Affected rows: " + i);
    }

    @SneakyThrows
    @Test
    void update() {
        var i = getStatement().executeUpdate("UPDATE workers SET emp_salary=50000 WHERE emp_id=100");
        System.out.println("Affected rows: " + i);
    }

    @SneakyThrows
    @Test
    void delete() {
        var i = getStatement().executeUpdate("DELETE FROM workers WHERE emp_salary > 1000");
        System.out.println("Affected rows: " + i);
    }
}
